module bitbucket.org/uwaploe/dpmon

go 1.21.5

require (
	bitbucket.org/mfkenney/go-redisq/v3 v3.2.0
	bitbucket.org/mfkenney/imm v0.6.0
	bitbucket.org/uwaploe/dpipc v0.1.0
	bitbucket.org/uwaploe/go-dpcmsg/v3 v3.0.0-beta.1
	bitbucket.org/uwaploe/immpkt v0.6.0
	bitbucket.org/uwaploe/tsfpga v0.7.1
	github.com/gomodule/redigo v1.8.9
	go.bug.st/serial v1.6.1
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	bitbucket.org/uwaploe/go-mpc v1.3.1 // indirect
	github.com/creack/goselect v0.1.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/snksoft/crc v1.1.0 // indirect
	github.com/vmihailenco/msgpack/v5 v5.4.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sys v0.0.0-20220829200755-d48e67d00261 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
