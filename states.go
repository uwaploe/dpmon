package main

import (
	"container/list"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net"
	"strings"
	"time"

	"bitbucket.org/mfkenney/imm"
	"bitbucket.org/uwaploe/dpmon/internal/power"
	"bitbucket.org/uwaploe/dpmon/internal/pubredis"
	"bitbucket.org/uwaploe/immpkt"
	"github.com/gomodule/redigo/redis"
)

// Maximum run of response decoding errors that will trigger a restart
const maxErrorRun uint = 10

// Redis pub-sub channels
const (
	eventsChan = "events.mmp"
	sciChan    = "data.sci"
	engChan    = "data.eng"
	respChan   = "response.dpc"
)

var ErrUnknown = errors.New("Invalid message prefix")
var ErrFormat = errors.New("Malformed response")
var ErrMax = errors.New("Maximum decoding errors exceeded")
var ErrNet = errors.New("No network access to profiler")

type sysState struct {
	dev           *imm.Device
	cfg           *progCfg
	peerSn        string
	err           error
	sw            power.Switch
	pnum          uint
	conn          redis.Conn
	cmds          cmdList
	ar            *archiver
	dataPub       *pubredis.Publisher
	evPub         *pubredis.Publisher
	respPub       *pubredis.Publisher
	ignoreTimeout bool
	errCount      uint
}

type saveState struct {
	Pnum  uint                 `json:"pnum"`
	Cache map[string]time.Time `json:"tscache"`
}

type queueEntry struct {
	Id   string `json:"id"`
	Body string `json:"body"`
}

func (s *sysState) Store(w io.Writer) error {
	enc := json.NewEncoder(w)
	return enc.Encode(saveState{Pnum: s.pnum, Cache: s.ar.ts})
}

func (s *sysState) Load(r io.Reader) error {
	state := saveState{}
	dec := json.NewDecoder(r)
	if err := dec.Decode(&state); err != nil {
		return err
	}

	s.pnum = state.Pnum
	s.ar.ts = state.Cache

	return nil
}

func (s *sysState) Setup(ctx context.Context) string {
	s.sw.On()
	slog.Info("Power on")
	time.Sleep(time.Duration(s.cfg.Warmup) * time.Millisecond)
	if err := s.dev.Wakeup(); err != nil {
		s.err = fmt.Errorf("No response from IMM: %w", err)
		return "error"
	}

	reply, err := s.dev.Exec("gethd")
	if err != nil {
		s.err = fmt.Errorf("No response from IMM: %w", err)
		return "error"
	}
	slog.Info("IMM ready", slog.String("s/n", reply.Hd.Sn))
	slog.Info("Configuring IMM")
	for k, v := range s.cfg.Settings {
		slog.Info("IMM parameter change", slog.String("key", k),
			slog.Any("value", v))
		if err := s.dev.Set(k, v); err != nil {
			s.err = fmt.Errorf("Cannot set %q: %w", k, err)
			return "error"
		}
	}

	// Run a coupler test
	slog.Info("Running ICC test")
	_, err = s.dev.Exec("fcl")
	if err != nil {
		s.err = fmt.Errorf("fcl command error: %w", err)
		return "error"
	}
	reply, err = s.dev.Exec("t20cc")
	if err != nil {
		s.err = fmt.Errorf("t20cc command error: %w", err)
		return "error"
	}

	slog.Info("ICC test complete",
		slog.String("status", reply.Tcc.Status),
		slog.Int("Z", reply.Tcc.Z))

	if s.peerSn == "" {
		return "discover"
	}

	slog.Info("Skipping peer discovery phase")
	return "mainloop"
}

// Check for the presence of a peer IMM and update the system state. Exits when
// either a peer is found, the maximum number of checks have been tried, or the
// context is canceled.
func (s *sysState) Discover(ctx context.Context) string {
	_, err := s.dev.Exec("fcl")
	if err != nil {
		s.err = fmt.Errorf("fcl command error: %w", err)
		return "error"
	}

	wait := time.Duration(s.cfg.Discover.Interval) * time.Millisecond
	for {
		slog.Info("Searching for DPC IMM")
		reply, err := s.dev.Exec("disc")
		if err != nil {
			s.err = fmt.Errorf("disc command error: %w", err)
			return "error"
		}

		if reply != nil {
			peers := reply.Peers()
			if len(peers) > 0 {
				slog.Info("Found peer(s)", slog.Any("s/n", peers))
				s.peerSn = peers[len(peers)-1]
				return "mainloop"
			}
		}
		s.cfg.Discover.Tries--
		if s.cfg.Discover.Tries <= 0 {
			s.err = errors.New("No peer found")
			return "error"
		}

		select {
		case <-time.After(wait):
		case <-ctx.Done():
			s.err = ctx.Err()
			return "error"
		}
	}
}

func (s *sysState) nextCommand() string {
	if cmd, err := redis.String(s.conn.Do("LPOP", s.cfg.Queues["command"])); err == nil {
		return cmd
	}

	return ""
}

type udpClient struct {
	conn  *net.UDPConn
	reply []byte
}

func newUdpClient(server string) *udpClient {
	addr, err := net.ResolveUDPAddr("udp4", server)
	if err != nil {
		slog.Error("resolve address", slog.Any("err", err),
			slog.String("addr", server))
		return nil
	}

	conn, err := net.DialUDP("udp4", nil, addr)
	if err != nil {
		slog.Error("server access", slog.Any("err", err), slog.String("addr", server))
		return nil
	}

	return &udpClient{conn: conn, reply: make([]byte, 1024)}
}

func (c *udpClient) Close() error {
	return c.conn.Close()
}

func (c *udpClient) Request(cmd string, timeout time.Duration) (immpkt.Message, error) {
	m := immpkt.Message{}
	if c == nil {
		return m, ErrNet
	}
	c.conn.SetDeadline(time.Now().Add(timeout))
	defer c.conn.SetDeadline(time.Time{})
	_, err := c.conn.Write([]byte(cmd))
	if err != nil {
		return m, err
	}

	rlen, err := c.conn.Read(c.reply)
	if err != nil {
		return m, err
	}

	err = m.UnmarshalBinary(c.reply[0:rlen])
	return m, err
}

type immClient struct {
	*imm.Client
}

func (c *immClient) Request(cmd string, timeout time.Duration) (immpkt.Message, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	resp, err := c.Client.Request(ctx, cmd)
	if err != nil {
		return immpkt.Message{}, err
	}
	defer resp.Close()
	dec := immpkt.NewDecoder(resp)

	return dec.Decode()
}

type Requestor interface {
	Request(string, time.Duration) (immpkt.Message, error)
}

func dpcQuery(cmd string, req Requestor, timeout time.Duration) (immpkt.Message, error) {
	return req.Request(cmd, timeout)
}

func (s *sysState) Mainloop(ctx0 context.Context) string {
	cln, err := s.dev.Connect(imm.DevSn(s.peerSn))
	if err != nil {
		s.err = fmt.Errorf("Cannot connect to peer: %w", err)
		return "error"
	}

	slog.Info("start session", slog.String("peer", s.peerSn))

	udpCln := newUdpClient(s.cfg.Server)
	immCln := &immClient{Client: cln}

	interval := time.Duration(10) * time.Second
	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	cmds := list.New()
	counter := uint(0)

	for {
		select {
		case <-ctx0.Done():
			s.err = ctx0.Err()
			return "error"
		case <-ticker.C:
		}

		slot := counter % 10
		counter++

		// Collect scheduled commands
		for _, c := range s.cmds[slot] {
			cmds.PushBack(c)
		}

		// Get the next command from the Redis queue
		if cmd := s.nextCommand(); cmd != "" {
			slog.Info("command from queue", slog.String("cmd", cmd))
			cmds.PushBack(cmd)
		}

		// Send each command and process the response
		for cmds.Len() > 0 {
			select {
			case <-ctx0.Done():
				s.err = ctx0.Err()
				return "error"
			default:
			}
			cmd := cmds.Remove(cmds.Front()).(string)
			// The commands from the Redis queue can be either
			// a JSON encoded queueEntry or simply a string.
			entry := queueEntry{}
			if err := json.Unmarshal([]byte(cmd), &entry); err != nil {
				entry.Body = cmd
			}

			// Send the command using the IMM link first then fallback to
			// UDP in case the vehicle is in the dock.
			msg, err := dpcQuery(entry.Body, immCln, time.Second*7)
			if err != nil {
				// IMM comms failed, try the network ...
				msg, err = dpcQuery(entry.Body, udpCln, time.Second*3)
				if err != nil {
					s.errCount++
					slog.Error("command failed", slog.Any("err", err),
						slog.String("text", entry.Body),
						slog.Int("err_count", int(s.errCount)))
					// After the maximum run of errors, exit the loop to
					// force a power-cycle of the IMM
					if s.errCount >= maxErrorRun {
						s.err = ErrMax
						return "error"
					} else {
						continue
					}
				}
			}
			s.errCount = 0

			// If the request was a queueEntry, publish the response using the
			// same ID.
			//nolint:errcheck
			if entry.Id != "" {
				s.respPub.Publish(respChan, queueEntry{Id: entry.Id, Body: string(msg.Payload)})
			}

			// Process the response
			var pnum uint
			err = nil
			switch msg.Class {
			case 'I', 'E':
				slog.Info("got response",
					slog.String("text", strings.TrimRight(string(msg.Payload), " \r\n")))
			case 'S':
				pnum, err = procStatus(msg.Payload, s.ar, s.dataPub)
				if pnum > s.pnum {
					newProfile(s.pnum, pnum, s.evPub)
					s.pnum = pnum
				}
			case 'D':
				err = procSensor(msg.Payload, s.ar, s.dataPub)
			case 'V':
				pnum, err = procEvent(msg.Payload, s.ar, s.evPub)
				if pnum > s.pnum {
					_ = s.evPub.Publish(eventsChan, msg.Payload)
					s.pnum = pnum
				}
			case 'B':
				err = procBattery(msg.Payload, s.ar, s.dataPub)
			}
			if err != nil {
				slog.Error("process response", slog.Any("err", err),
					slog.Any("class", msg.Class))
			}
		}
	}
}

func (s *sysState) Shutdown(ctx context.Context) string {
	if _, err := s.dev.Exec("rel"); err != nil {
		slog.Error("release IMM line", slog.Any("err", err))
	}

	if err := s.dev.Sleep(); err != nil {
		slog.Error("power down IMM", slog.Any("err", err))
	}
	time.Sleep(time.Millisecond * 250)
	s.sw.Off()
	slog.Info("Power off")
	return "done"
}

func (s *sysState) Err(ctx context.Context) string {
	if s.err != nil {
		slog.Error("Error state", slog.Any("cause", s.err))
	}

	return "shutdown"
}
