package main

import (
	"fmt"
	"log/slog"
	"time"

	redisq "bitbucket.org/mfkenney/go-redisq/v3"
	"bitbucket.org/uwaploe/dpipc"
	dpcmsg "bitbucket.org/uwaploe/go-dpcmsg/v3"
	"google.golang.org/protobuf/proto"
)

type archiver struct {
	dataq *redisq.Queue[dpipc.DataRecord]
	ts    map[string]time.Time
}

type Publisher interface {
	// Publish a message to a topic (channel). Data is JSON encoded
	// before publishing.
	Publish(topic string, data interface{}) error
}

//nolint:errcheck
func newProfile(prev, next uint, p Publisher) {
	// Signal the end of the current profile ...
	ev := dpipc.Event{
		Name:  "profile:end",
		Attrs: make(map[string]interface{})}
	ev.Attrs["t"] = int64(time.Now().Unix())
	ev.Attrs["pnum"] = int64(prev)
	slog.Info("end profile", slog.Any("pnum", ev.Attrs["pnum"]))
	p.Publish(eventsChan, ev)

	// ... and the start of a new one.
	ev.Name = "profile:start"
	ev.Attrs["pnum"] = int64(next)
	slog.Info("start profile", slog.Any("pnum", ev.Attrs["pnum"]))
	p.Publish(eventsChan, ev)
}

func procStatus(buf []byte, ar *archiver, p Publisher) (uint, error) {
	status := &dpcmsg.Status{}

	if err := proto.Unmarshal(buf, status); err != nil {
		return 0, fmt.Errorf("Protobuf decode failed: %w", err)
	}

	dr := status.MarshalDataRecord()
	// Send data record to the archive
	if err := ar.dataq.Put(dr); err != nil {
		slog.Error("archive data record", slog.Any("err", err),
			slog.String("src", dr.Src))
	}

	//nolint:errcheck
	if p != nil {
		p.Publish(engChan, dr)
	}

	return uint(status.Profile), nil
}

func procSensor(buf []byte, ar *archiver, p Publisher) error {
	sd := &dpcmsg.SensorData{}

	if err := proto.Unmarshal(buf, sd); err != nil {
		return fmt.Errorf("Protobuf decode failed: %w", err)
	}

	recs := sd.ToDataRecords()
	for _, dr := range recs {
		// Discard any record which has a timestamp earlier than
		// the one in the cache.
		if !dr.T.After(ar.ts[dr.Src]) {
			continue
		}
		// Try to screen out bogus timestamps
		dt := time.Since(dr.T).Seconds()
		if dt > 86400 || dt < -86400 {
			continue
		}

		// Cache the timestamp
		ar.ts[dr.Src] = dr.T
		// Send data record to the archive
		if err := ar.dataq.Put(dr); err != nil {
			slog.Error("archive data record", slog.Any("err", err),
				slog.String("src", dr.Src))
		}

		//nolint:errcheck
		if p != nil {
			p.Publish(sciChan, dr)
		}
	}

	return nil
}

func procBattery(buf []byte, ar *archiver, p Publisher) error {
	bat := &dpcmsg.BatterySummary{}

	if err := proto.Unmarshal(buf, bat); err != nil {
		return fmt.Errorf("Protobuf decode failed: %w", err)
	}

	dr := bat.MarshalDataRecord()
	if !dr.T.After(ar.ts[dr.Src]) {
		return nil
	}

	// Try to screen out bogus timestamps
	dt := time.Since(dr.T).Seconds()
	if dt > 86400 || dt < -86400 {
		return nil
	}

	// Cache the timestamp
	ar.ts[dr.Src] = dr.T
	// Send data record to the archive
	if err := ar.dataq.Put(dr); err != nil {
		slog.Error("archive data record", slog.Any("err", err),
			slog.String("src", dr.Src))
	}

	//nolint:errcheck
	if p != nil {
		p.Publish(engChan, dr)
	}

	return nil
}

func procEvent(buf []byte, ar *archiver, p Publisher) (uint, error) {
	ev := dpipc.ParseEvent(string(buf))
	secs, ok := ev.Attrs["t"].(int64)
	if !ok {
		return 0, fmt.Errorf("Cannot parse event message: %q", buf)
	}

	tstamp := time.Unix(secs, int64(0))
	if !tstamp.After(ar.ts["evlogger"]) {
		return 0, nil
	}

	// Try to screen out bogus timestamps
	dt := time.Since(tstamp).Seconds()
	if dt > 86400 || dt < -86400 {
		return 0, nil
	}

	ar.ts["evlogger"] = tstamp
	if ev.Name == "profile:start" {
		// Profile start events are published by the caller after checking
		// the profile number against the current one.
		return uint(ev.Attrs["pnum"].(int64)), nil
	}

	//nolint:errcheck
	p.Publish(eventsChan, ev)

	return 0, nil
}
