package main

import (
	"encoding"
	"encoding/json"
	"fmt"
)

func FmtText(v interface{}) ([]byte, error) {
	switch t := v.(type) {
	case encoding.TextMarshaler:
		return t.MarshalText()
	case fmt.Stringer:
		return []byte(t.String()), nil
	case []byte:
		return t, nil
	case string:
		return []byte(t), nil
	default:
		return []byte(fmt.Sprintf("%#v", v)), nil
	}
}

func FmtJSON(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}
