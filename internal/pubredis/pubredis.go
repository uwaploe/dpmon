// Package pubredis publishes messages to a Redis pub-sub server.
package pubredis

import (
	"github.com/gomodule/redigo/redis"
)

type Formatter func(interface{}) ([]byte, error)

type Publisher struct {
	conn redis.Conn
	f    Formatter
}

// Publish implements the Publisher interface.
func (p *Publisher) Publish(topic string, data interface{}) error {
	b, err := p.f(data)
	if err != nil {
		return err
	}
	_, err = p.conn.Do("PUBLISH", topic, b)
	return err
}

// New creates a new Publisher which uses a Redis data-store to publish
// messages formatted by f.
func New(conn redis.Conn, f Formatter) *Publisher {
	return &Publisher{
		conn: conn,
		f:    f,
	}
}
