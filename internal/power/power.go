// Power provides a power switch interface using the TSFPGA gPRC server
package power

import (
	"context"
	"strings"
	"time"

	pb "bitbucket.org/uwaploe/tsfpga/api"
	"google.golang.org/grpc"
)

type Switch interface {
	On() error
	Off() error
	Test() (bool, error)
}

func dioset(client pb.FpgaMemClient, name string, state pb.DioState) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{
		Name:  name,
		State: state}
	_, err := client.DioSet(ctx, msg)
	return err
}

func dioget(client pb.FpgaMemClient, name string) (pb.DioState, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{Name: name}
	resp, err := client.DioGet(ctx, msg)
	if err != nil {
		return pb.DioState_LOW, err
	}
	return resp.State, nil
}

type Dio struct {
	name   string
	client pb.FpgaMemClient
}

func NewDio(conn *grpc.ClientConn, name string) *Dio {
	return &Dio{
		name:   strings.ToUpper(name),
		client: pb.NewFpgaMemClient(conn),
	}
}

func (d *Dio) On() error {
	state := pb.DioState_value["HIGH"]
	return dioset(d.client, d.name, pb.DioState(state))
}

func (d *Dio) Off() error {
	state := pb.DioState_value["LOW"]
	return dioset(d.client, d.name, pb.DioState(state))
}

func (d *Dio) Test() (bool, error) {
	state, err := dioget(d.client, d.name)
	return (state == pb.DioState_HIGH), err
}
