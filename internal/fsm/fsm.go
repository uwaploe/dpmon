// Package fsm implements a finite state machine
package fsm

import (
	"context"
	"errors"
	"fmt"
	"log"
)

var ErrState = errors.New("Invalid FSM state")

type Handler func(context.Context) string

type Fsm struct {
	start    string
	handlers map[string]Handler
	end      map[string]bool
}

func New(start string) *Fsm {
	return &Fsm{
		start:    start,
		handlers: make(map[string]Handler),
		end:      make(map[string]bool),
	}
}

func (fsm *Fsm) AddState(name string, fn Handler) {
	fsm.handlers[name] = fn
}

func (fsm *Fsm) AddEndState(name string) {
	fsm.end[name] = true
}

func (fsm *Fsm) Run(ctx context.Context) error {
	handler := fsm.handlers[fsm.start]
	for {
		next := handler(ctx)
		log.Printf("Next state = %q", next)
		if _, done := fsm.end[next]; done {
			return nil
		}
		handler = fsm.handlers[next]
		if handler == nil {
			return fmt.Errorf("%s: %w", next, ErrState)
		}
	}
}
