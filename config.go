package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

var defaultCfg = progCfg{
	Port: portCfg{
		Device:  "/dev/ttymxc2",
		Baud:    9600,
		Switch:  "131",
		Timeout: 5000,
	},
	Warmup: 1000,
	Server: "profiler:4200",
	Discover: discCfg{
		Interval: 10000,
		Tries:    180,
	},
	Settings: map[string]interface{}{
		"thost0":                  0,
		"thost1":                  0,
		"thost2":                  500,
		"thost3":                  1000,
		"tmodem2":                 600,
		"tmodem3":                 1100,
		"enableecho":              0,
		"enablebinarydata":        1,
		"enablehostwakeupcr":      0,
		"enablehostpromptconfirm": 0,
		"enablefullpwrtx":         1,
		"enablebackspace":         0,
		"termtohost":              254,
		"termfromhost":            254,
	},
	Queues: map[string]string{
		"command": "dpc:commands",
		"data":    "archive:queue",
	},
	Commands: []cmdCfg{
		{
			Text:  "status",
			Slots: []uint{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		},
		{
			Text:  "ev",
			Slots: []uint{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		},
		{
			Text:  "sens ctd_1 optode_1 battavg",
			Slots: []uint{0, 2, 4, 6, 8},
		},
		{
			Text:  "sens acm_1 flntu_1 flcd_1",
			Slots: []uint{1, 3, 5, 7, 9},
		},
	},
}

type progCfg struct {
	Port     portCfg
	Server   string
	Warmup   int
	Discover discCfg
	// IMM configuration settings
	Settings map[string]interface{}
	// Redis queues (lists)
	Queues map[string]string
	// Scheduled commands
	Commands []cmdCfg
}

// Peer discovery settings
type discCfg struct {
	// Check interval in milliseconds
	Interval int `yaml:"interval"`
	// Number of attempts before giving up
	Tries int `yaml:"tries"`
}

type portCfg struct {
	Device  string `yaml:"device"`
	Baud    int    `yaml:"baud"`
	Switch  string `yaml:"switch"`
	Timeout int    `yaml:"timeout"`
}

// Commands for the DPC
type cmdCfg struct {
	Text  string `yaml:"text"`
	Slots []uint `yaml:"slots"`
}

type cmdList [10][]string

func (cfg progCfg) makeCmdList() cmdList {
	var cl cmdList
	for _, c := range cfg.Commands {
		for _, t := range c.Slots {
			if t < uint(len(cl)) {
				if cl[t] == nil {
					cl[t] = make([]string, 1)
					cl[t][0] = c.Text
				} else {
					cl[t] = append(cl[t], c.Text)
				}
			}
		}
	}

	return cl
}

func (cl cmdList) Log() {
	log.Println("Scheduled commands")
	for i, c := range cl {
		log.Printf("[%d]=%q\n", i, c)
	}
}

func (cl cmdList) String() string {
	var b strings.Builder
	for i, c := range cl {
		fmt.Fprintf(&b, "[%d]=%q ", i, c)
	}

	return b.String()
}

func InitConfig(path string) (progCfg, error) {
	cfg := defaultCfg
	if path != "" {
		contents, err := os.ReadFile(path)
		if err != nil {
			return cfg, err
		}
		err = cfg.Load(contents)
		if err != nil {
			return cfg, err
		}
	}
	return cfg, nil
}

func (c *progCfg) Load(contents []byte) error {
	return yaml.Unmarshal(contents, c)
}

func (c progCfg) Dump(w io.Writer) error {
	enc := yaml.NewEncoder(w)
	enc.Encode(c)
	return enc.Close()
}
