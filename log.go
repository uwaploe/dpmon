package main

import (
	"os"

	"log/slog"
)

func initLogger() *slog.Logger {
	opts := slog.HandlerOptions{}
	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		opts.ReplaceAttr = func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return a
		}
	}

	return slog.New(slog.NewTextHandler(os.Stderr, &opts))
}

func abort(msg string, err error, args ...any) {
	newargs := append([]any{slog.Any("err", err)}, args...)
	slog.Error(msg, newargs...)
	os.Exit(1)
}
