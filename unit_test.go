package main

import (
	"fmt"
)

func Example_progCfg_makeCmdList() {
	cfg, _ := InitConfig("")
	cl := cfg.makeCmdList()
	fmt.Println(cl)
	// Output:
	// [0]=["status" "ev" "sens ctd_1 optode_1 battavg"] [1]=["status" "ev" "sens acm_1 flntu_1 flcd_1"] [2]=["status" "ev" "sens ctd_1 optode_1 battavg"] [3]=["status" "ev" "sens acm_1 flntu_1 flcd_1"] [4]=["status" "ev" "sens ctd_1 optode_1 battavg"] [5]=["status" "ev" "sens acm_1 flntu_1 flcd_1"] [6]=["status" "ev" "sens ctd_1 optode_1 battavg"] [7]=["status" "ev" "sens acm_1 flntu_1 flcd_1"] [8]=["status" "ev" "sens ctd_1 optode_1 battavg"] [9]=["status" "ev" "sens acm_1 flntu_1 flcd_1"]
}
