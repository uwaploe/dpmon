# DP Monitor Program

This program manages the Inductive Modem (IMM) interface on the Docking Station Controller (DSC). It sends commands to the Deep Profiler Controller (DPC) on a periodic basis and processes the responses. It requires the [Redis](https://redis.io) data-store server for communication with other processes on the system.

## Installation

Fetch the latest compressed TAR archive from the [Downloads](https://bitbucket.org/uwaploe/dpmon/downloads/) section of this repository and copy to `~rsn/stow` directory on the DSC, unpack and install as shown below (set VERS to the program version):

``` shellsession
$ cd ~/stow
$ tar -x -z -f dpmon_${VERS}_linux_arm5.tar.gz
$ cd dpmon_${VERS}_linux_arm5
$ ./scripts/install.sh
`/home/rsn/bin/dpmon' -> `/home/rsn/stow/dpmon_0.2.1_linux_armv5/dpmon'
`runit/dpmon/log/run' -> `/home/rsn/sv/dpmon/log/run'
`runit/dpmon/run' -> `/home/rsn/sv/dpmon/run'
$ sv restart ~/service/dpmon
ok: run: /home/rsn/service/dpmon: (pid 14959) 1s
```

## Operations

This program is run as a service by the supervisor program, [Runit](http://smarden.org/runit/index.html), so it can be automatically restarted when it exits.

1. Power-on IMM.
2. Configure IMM.
3. Enter peer-discovery loop. Exit the loop when the peer IMM is detected,
   exit the program on timeout.
4. Enter main loop.

The IMM is powered-off when the program exits.

The diagnostic output is logged to a series of rotation files in `~rsn/logs/dpmon/`. The latest log is `~rsn/logs/dpmon/current`.

### Configuration

The default program configuration can be written to standard output with the `--dumpcfg` command-line option. To change the configuration, write the defaults to the file `~rsn/config/dpmon.yaml`, edit as necessary and restart the service. This file is automatically read at startup if it exists.

``` shellsession
$ dpmon --dumpcfg > ~/config/dpmon.yaml
[ ...edit file... ]
$ sv restart ~/service/dpmon
```

### Main loop

In this state, the program sends one or more scheduled commands to the DPC every 10 seconds. The scheduled commands are specified in the configuration file as shown below. A command can be listed in up to 10 *slots*. Each slot is a 10-second interval so a command listed in every slot will be sent every 10 seconds while a command listed in one slot will be sent every 100 seconds.

``` yaml
# List of scheduled commands. Each entry consists of a command to send to
# the DPC and the slot (command interval)in which to send the
# command. Valid slot numbers are 0-9
commands:
  - text: status
    slots: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  - text: ev
    slots: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  - text: sens ctd_1 optode_1
    slots: [0, 2, 4, 6, 8]
  - text: sens acm_1 flntu_1 flcd_1
    slots: [1, 3, 7, 9]
```

In addition to the scheduled commands, the program reads an entry from the command queue (FIFO), implemented as the Redis list *dpc:commands*, and sends it to the DPC. The command is then removed from the list. Other processes may add entries to the list using the Redis commands *RPUSH* (add to the list tail) or *LPUSH* (add to the list head).

Each entry in the queue can be either a string or a [JSON](http://json.org) encoded object with two attributes; *id*, a string containing a unique ID for the command and *body*, a string containing the contents of the command.

Any communication error with the IMM will force the program to exit. A restart by the supervisor process results in power-cycling the IMM which will hopefully resolve any transient issues.
