// Dpmon monitors and controls the Deep Profiler vehicle via an inductive
// modem communication link.
package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"log/slog"

	redisq "bitbucket.org/mfkenney/go-redisq/v3"
	"bitbucket.org/mfkenney/imm"
	"bitbucket.org/uwaploe/dpipc"
	"bitbucket.org/uwaploe/dpmon/internal/fsm"
	"bitbucket.org/uwaploe/dpmon/internal/power"
	"bitbucket.org/uwaploe/dpmon/internal/pubredis"
	"github.com/gomodule/redigo/redis"
	"go.bug.st/serial"
	"google.golang.org/grpc"
)

const Usage = `Usage: dpmon [options] [cfgfile]

Control and monitor the Deep Profiler vehicle via an inductive modem
communication link.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpCfg = flag.Bool("dumpcfg", false,
		"Dump default configuration to stdout and exit")
	stateFile string
	rpcAddr   string = "localhost:10101"
	rdAddr    string = "localhost:6379"
	notStrict bool
	peerSn    string
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr,
		"host:port for power control gRPC server")
	flag.StringVar(&rdAddr, "rd-addr", rdAddr,
		"host:port for Redis server")
	flag.StringVar(&stateFile, "state", stateFile,
		"File in which to save state information")
	flag.BoolVar(&notStrict, "not-strict", notStrict,
		"Ignore timeouts for DPC responses")
	flag.StringVar(&peerSn, "peer", peerSn,
		"Set peer IMM serial number")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpCfg {
		cfg, _ := InitConfig("")
		cfg.Dump(os.Stdout)
		os.Exit(0)
	}

	return flag.Args()
}

func loadState(filename string, state *sysState) {
	f, err := os.Open(filename)
	if err != nil {
		return
	}
	defer f.Close()
	state.Load(f)
}

func storeState(filename string, state *sysState) {
	f, err := os.Create(filename)
	if err != nil {
		return
	}
	defer f.Close()
	state.Store(f)
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func openSerialPort(cfg portCfg) (serial.Port, error) {
	mode := &serial.Mode{
		BaudRate: cfg.Baud,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	port, err := serial.Open(cfg.Device, mode)
	if err != nil {
		return port, fmt.Errorf("open %s: %w", cfg.Device, err)
	}

	if cfg.Timeout > 0 {
		_ = port.SetReadTimeout(time.Duration(cfg.Timeout) * time.Millisecond)
	}

	return port, nil
}

func main() {
	_ = parseCmdLine()

	slog.SetDefault(initLogger())

	cfg, err := InitConfig(flag.Arg(0))
	if err != nil {
		abort("Cannot parse config file", err)
	}

	state := &sysState{
		cfg:           &cfg,
		cmds:          cfg.makeCmdList(),
		peerSn:        peerSn,
		ignoreTimeout: notStrict}

	// Power switch
	if gconn, err := grpcConnect(rpcAddr); err != nil {
		abort("TS-FPGA connect", err, slog.String("addr", rpcAddr))
	} else {
		state.sw = power.NewDio(gconn, cfg.Port.Switch)
		// Make sure the power is off
		state.sw.Off()
	}

	// Redis server
	if state.conn, err = redis.Dial("tcp", rdAddr); err != nil {
		abort("Redis connect", err, slog.String("addr", rdAddr))
	}

	// Serial port
	port, err := openSerialPort(cfg.Port)
	if err != nil {
		abort("open serial device", err, slog.String("dev", cfg.Port.Device))
	}
	state.dev = imm.NewDevice(port, imm.ModeIMService)

	// Queue for data archiver
	state.ar = &archiver{ts: make(map[string]time.Time)}
	state.ar.dataq = redisq.NewQueue[dpipc.DataRecord](state.conn, cfg.Queues["data"], 200)

	// Publishers
	state.dataPub = pubredis.New(state.conn, FmtJSON)
	state.evPub = pubredis.New(state.conn, FmtText)
	state.respPub = pubredis.New(state.conn, FmtJSON)

	if stateFile != "" {
		loadState(stateFile, state)
		defer storeState(stateFile, state)
	}

	machine := fsm.New("setup")
	machine.AddState("setup", state.Setup)
	machine.AddState("discover", state.Discover)
	machine.AddState("mainloop", state.Mainloop)
	machine.AddState("error", state.Err)
	machine.AddState("shutdown", state.Shutdown)
	machine.AddEndState("done")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close. If a signal arrives, close the "cancel" channel
	// to cleanly shutdown the FSM.
	go func() {
		s, more := <-sigs
		if more {
			slog.Info("interrupt", slog.String("sig", s.String()))
			cancel()
		}
	}()

	slog.Info("Deep Profiler monitor", slog.String("vers", Version))

	if err := machine.Run(ctx); err != nil {
		slog.Error("", slog.Any("err", err))
	}

}
